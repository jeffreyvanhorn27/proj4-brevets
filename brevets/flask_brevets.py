"""
Replacement for RUSA ACP brevet time calculator
(see https://rusa.org/octime_acp.html)

"""

import flask
from flask import request
import arrow  # Replacement for datetime, based on moment.js
import acp_times  # Brevet time calculations
import config
import os

import logging

###
# Globals
###
app = flask.Flask(__name__)
CONFIG = config.configuration()
#app.secret_key = CONFIG.SECRET_KEY
app.secret_key = os.urandom(24)
###
# Pages
###


@app.route("/")
@app.route("/index")
def index():
    app.logger.debug("Main page entry")
    return flask.render_template('calc.html')


@app.errorhandler(404)
def page_not_found(error):
    app.logger.debug("Page not found")
    flask.session['linkback'] = flask.url_for("index")
    return flask.render_template('404.html'), 404


###############
#
# AJAX request handlers
#   These return JSON, rather than rendering pages.
#
###############
@app.route("/_calc_times")
def _calc_times():
    """
    Calculates open/close times from miles, using rules
    described at https://rusa.org/octime_alg.html.
    Expects one URL-encoded argument, the number of miles.
    """
    app.logger.debug("Got a JSON request")
    km = request.args.get('km', 999, type=float)
    bl = request.args.get('bl', 999, type=int)
    bt = request.args.get('bt', "", type=str)
    print("bl: {}".format(bl))
    print("bt: {}".format(bt))
    app.logger.debug("km={}".format(km))
    app.logger.debug("request.args: {}".format(request.args))
    # FIXME: These probably aren't the right open and close times
    # and brevets may be longer than 200km

    begin_time = arrow.get(bt)
    print("arrow made: {}".format(begin_time))

    open_time = acp_times.open_time(km, bl, begin_time)
    close_time = acp_times.close_time(km, bl, begin_time)
    #fix the open_time
    #get the length of the brevet
    """
    brev_length = 
    open_time = acp_times.open_time(km, )
    """
    result = {"open": open_time, "close": close_time}
    return flask.jsonify(result=result)


#############

app.debug = CONFIG.DEBUG
if app.debug:
    app.logger.setLevel(logging.DEBUG)

if __name__ == "__main__":
    app.debug = True
    print("Opening for global access on port {}".format(CONFIG.PORT))
    print(acp_times.close_time(0, 400, arrow.get("2017-02-01T09:30")))
    app.run(port=CONFIG.PORT, host="0.0.0.0")
