from acp_times import open_time
from acp_times import close_time
import arrow

import nose
import logging
logging.basicConfig(format='%(levelname)s:%(message)s',
                    level=logging.WARNING)
log = logging.getLogger(__name__)

def test_acp_times_start():
	#logger.debug("output {}".format(close_time(0, 400, arrow.get("2017-02-01T09:30"))))
	assert (close_time(0, 400, arrow.get("2017-02-01T09:30")) == "2017-02-01T10:30:00+00:00")

def test_acp_times_negative():
	assert(open_time(-10, 600, arrow.get("2018-03-04T12:15")) == None and close_time(-10, 600, arrow.get("2018-03-04T12:15")) == None)

def test_acp_times_too_far():
	assert(open_time(270, 200, arrow.get("2018-03-04T12:15")) == None and close_time(270, 200, arrow.get("2018-03-04T12:15")) == None)

def test_acp_times_past_finish():
	assert(close_time(410, 400, arrow.get("2020-07-24T00:00")) == "2020-07-25T03:00:00+00:00")

def test_acp_times_open_time():
	assert(open_time(350, 600, arrow.get("2017-06-10T10:00")) == "2017-06-10T20:34:00+00:00")

def test_acp_times_close_time():
	assert(close_time(550, 1000, arrow.get("2018-09-5T06:30")) == "2018-09-06T19:10:00+00:00")

def test_acp_times_letter():
	assert(open_time("a", 600, arrow.get("2018-03-04T12:15")) == None and close_time("a", 600, arrow.get("2018-03-04T12:15")) == None)

def test_acp_times_french():
	assert(close_time(20, 200, arrow.get("2020-01-01T00:00")) == "2020-01-01T02:00:00+00:00")	