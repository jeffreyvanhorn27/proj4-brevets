# Project 4: Brevet time calculator with Ajax

This application generates open and close times for controls in a brevet. 

## What is a Brevet?

A Brevet is a race that features a series of controls, similar to check points, that riders must reach between specified opening and close times. These races can be 200, 300, 400, 600 or 1,000 km in overall length with as many controls as desired.

## How are Control Times Calculated?

Opening times for each control are calculated using the maximum speed for the distance within which the control resides. Alternatively, close times are calculated using the minimum speeds for the distance within which the control resides. An important thing to keep in mind is that for control at longer distances, such as 350 km, multiple speeds may need tp be used. For example, in order to calculate the open time of a control at 350 km, we use a maximum speed of 34 km/hr for the first 200 km and a maximum speed of 32 km for the last 150 km. This gives us a calcultion of 200/34 + 150/32 = 5H53 + 4H41 = 10H34. A more detailed explanation, and listing of maximum and minimum speeds can be found here (https://rusa.org/pages/acp-brevet-control-times-calculator).

## Rules to Consider

Beyond the calculation of control opening and closing times, there are a few rules that must be kept in mind.

1: The distance of a control may never be 20% of the brevet's total length beyond what is supposed to be the total length of the brevet. For example, in a 200 km brevet a control at 205 km would be valid while a control at 250 km would not be. These controls that are beyond the specified total length have specificied close times, outlined here in Article 9 (https://rusa.org/pages/rulesForRiders)

2: The first control, at 0 km, must have a close time one hour after the start of the brevet.

3: In accoradance with French rules, for the first 60 km the maximum speed if 20 km/hr. The close time is then calculated by adding an hour to control distance divided by 20 km/hr.

## How to Run

Run the docker file inside the brevets directory using docker build -t "name" . and then run using docker run -d -p 5000:5000 name. The program can be stopped using docker stop id.

## For developers
This project can be extended primarily by modifying the open_time and close_time methods in acp_times.py. These two functions both use the calculate_time method in acp_times.py, which provides a calculation for both open and close times at a specified distance. In order to alter the front end, calc.html should be edited as it contains both the html for the project and the javascript, JQuery and AJAX that powers the page. Lastly, the credentials.ini file included is merely boiler plate and should be altered for your own specific implementation.

## Testing
This project uses the nose framwork for python. All tests are within the test_acp_times.py file and can be run from the brevets directory with the command nosetests. to run this command with the docker container make sure to use the docker exec command. If nosetests has difficulty running in Docker, specifically on Windows, use --exe flag when running docker exec.

## Contact Information
Name: Jeffrey Van Horn
Email: jeffreyv@uoregon.edu
